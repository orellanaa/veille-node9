# veille-node9

Utiliser AJAX pour modifier la collection de membres
• Les fonctionnalités AJAX à développer
o Retirer un membre de la liste
 La route /ajax_detruire permettra de retirer un membre de la
collection
 Si le membre a été bien retirer, la route renvoie un message (_id) au
client pour faire disparaître du tableau la ligne contenant le membre
qui vient juste d'être détruit.
o Ajouter un membre
 La route /ajax_ajouter permettra d'ajouter un membre à la
collection
 Une ligne vide apparaîtra dans le tableau pour permettre de remplir
les cellules
o Modifier un membre
 La route /ajax_modifier permettra de modifier le profil d'un
membre
